<?php
function theme_styles() {
	wp_enqueue_style('cwv-main', get_template_directory_uri() . '/assets/css/main.css', array(), filemtime(get_template_directory() . '/assets/css/main.css'), false);
}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

?>