<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and through the <header>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Charleston, WV Restaurant Week</title>
  <meta name="description" content="1 Week, Jan. 28 – Feb 2, 2019. 24 restaurants. 
3 course meals at $25 and $35 price points.">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="apple-touch-icon" href="<?= get_template_directory_uri()?>/icon.png">
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri()?>/icon.png" />
  <link href="https://fonts.googleapis.com/css?family=Archivo:400,600|Permanent+Marker" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" integrity="sha256-gVCm5mRCmW9kVgsSjQ7/5TLtXqvfCoxhdsjE6O1QLm8=" crossorigin="anonymous" />
<?php wp_head(); ?>
</head>

<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <header>
      <span class="mobile-bg">
        <a href="/">Menus</a>
        <a href="/blog" class="mobile_show">Blogs</a>
      </span>
      <img src="<?= get_template_directory_uri()?>/assets/img/logo_2020.png" alt="Charleston, WV Restaurant Week">
      
        <a href="/blog" class="desktop_show">Blogs</a>
      </span>
  </header>