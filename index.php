<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

$args = array(
  'post_type' => 'restaurant',
  'orderby' => 'title',
  'order'   => 'ASC',
  'posts_per_page' => -1
  );
$restaurant_query = new WP_Query( $args );
$restaurants = $restaurant_query->get_posts();

?>
  <div class="great-white-box">
    <div class="title-container">  
      <h1>The Restaurants</h1>
      <p>Click on a restaurant to see their menu.</p>
    </div>
    <div class="buttons">
      <div class="left">
          <button class="show-all selected">All</button>
          <button class="show-25">$25</button>
          <button class="show-35">$35</button>
      </div>
      <div class="right">
          <button class="random">See a Random Menu!</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="menus">

      <?php
        foreach ($restaurants as $post) {
          $postId = $post->ID;
          $cost = get_field('cost', $postId);
          $title = $post->post_title;
          $menu = get_field('menu', $postId);
          $headline = get_field('headline', $postId);
          $subtext = get_field('subtext', $postId);
          $encodedTitle = urlencode($title);
          $url = "?r=" . $encodedTitle . "&id=" . $postId;
      ?>
        <span class="rest-holder rest-<?= $postId ?> cost-<?= $cost ?>" data-id=<?= $postId ?>><a href="<?= $url ?>"><?= $title ?></a></span>
      <?php } ?>
    </div>

  <?php
    foreach ($restaurants as $post) {
      $postId = $post->ID;
      $menu = get_field('menu', $postId);
      $headline = get_field('headline', $postId);
      $subtext = get_field('subtext', $postId);
      $googleMaps = (get_field('google_maps_link', $postId)) ?? '';
      $phone = (get_field('phone_number', $postId)) ?? '';
      $circle = ' ○ ';
  ?>
  
<?php if( get_field('menu_ready') ): ?>

    <div class="rest-info rest-<?= $postId ?>">
      <p class="location"><?= $phone ?><?= $circle ?><a href="<?= $googleMaps ?>">Open in Google Maps</a></p>

      <section class="menu__course">
        <h2 class="label-above"><?= $headline ?></h2>
        <p><?= $subtext ?></p>
        <h3 class="label-above">Appetizer</h3>
          <ul>
            <li><?= $menu['app_1'] ?></li>
            <li><?= $menu['app_2'] ?></li>
          </ul>
      </section>

      <section class="menu__course">
        <h3 class="label-above">Entree</h3>
        <ul>
          <li><?= $menu['entree_1'] ?></li>
          <li><?= $menu['entree_2'] ?></li>
        </ul>
      </section>

      <section class="menu__course">
        <h3 class="label-above">Dessert</h3>
        <ul>
          <li><?= $menu['dessert_1'] ?></li>
          <li><?= $menu['dessert_2'] ?></li>
        </ul>
      </section>
    </div>

<?php else: ?>

    <div class="rest-info rest-<?= $postId ?>">
      <p class="location"><?= $phone ?><?= $circle ?><a href="<?= $googleMaps ?>">Open in Google Maps</a></p>

      <section class="menu__course">
        <h2 class="label-above">This menu is still being crafted. Please check back soon!</h2>
      </section>
    </div>

<?php endif; ?>

  <?php } ?>


  </div>
<?php get_footer();