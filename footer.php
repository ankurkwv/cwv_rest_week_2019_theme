<?php
/**
 * The template for displaying the footer
 *
 * Contains the whole footer and many scripts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
  <footer>
    <p>Restaurant Week is January 27 – February 1, 2020.</p>
    <p>3 courses at $25 and $35 price points.</p>
    <img src="<?= get_template_directory_uri()?>/assets/img/buzz.svg" alt="Buzz Sponsor Logo">
    <p class="sponsor">Main Sponsor</p>
    <p class="website">Charleston, West Virginia. Website by <a href="https://oakwood.digtial">Oakwood Digital</a>.</p>
  </footer>
  <!-- Scripts Before End of Body -->
  <script src="<?= get_template_directory_uri()?>/assets/js/vendor/modernizr-3.6.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.4/dist/sweetalert2.all.min.js" integrity="sha256-NIm6BOZtB0qD6ycn3fvFeJPgC81Wb0AmoXaerPdyd6Q=" crossorigin="anonymous"></script>
  <script src="<?= get_template_directory_uri()?>/assets/js/main-min.js"></script>
  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-38569922-7', 'auto'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>

<?php wp_footer(); ?>

</body>

</html>
