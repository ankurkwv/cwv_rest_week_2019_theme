<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); 

$args = array(
  'post_type' => 'blog',
  'orderby' => 'date',
  'order'   => 'DESC',
  );
$blog_query = new WP_Query( $args );
$blogs = $blog_query->get_posts();

?>
  <div class="great-white-box">
      <?php
        foreach ($blogs as $post) {
          $postId = $post->ID;
          $excerpt = $post->post_excerpt;
          $title = $post->post_title;
          $authorId = $post->post_author;
          $author = get_the_author_meta( 'display_name' , $authorId );
          $date = get_the_date('F j, Y', $postId);
      ?>
        <div class="blog-listing">  
          <h2 class="left"><a href="<?= get_permalink($postId) ?>"><?= $title ?></a></h2>
          <p><?= $excerpt ?></p>
          <p class="read-more"><a href="<?= get_permalink($postId) ?>" class="btn selected">Read the Blog</a></p>
        </div>

      <?php } ?>

    </div>


  </div>
<?php get_footer();