var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();

/**
 *
 * Opening and closing modals
 * containing menus,
 * rewriting URLS
 *
 */

$(document).ready(function() {

	if (urlParams['id'] != null) {
		var current_rest_name = $('.rest-holder.rest-'+urlParams['id']).children().first().text();
		var current_rest_id = urlParams['id'];
		var current_rest_html = $('.rest-info.rest-'+current_rest_id).html();
		setTimeout(function() {
			openSwal(current_rest_name, current_rest_html);
		}, 300);
	}

	$('.rest-holder').on('click', function(e) {
		console.log('here');
		e.preventDefault();
		var current_rest_name = $(this).children().first().text();
		var current_rest_id = $(this).data('id');
		var current_rest_html = $('.rest-info.rest-'+current_rest_id).html();

		var fake_url = '/?r=' + encodeURI(current_rest_name) + '&id=' + current_rest_id;
		window.history.pushState('', current_rest_name, fake_url);

		openSwal(current_rest_name, current_rest_html);

	});

	function openSwal(name, html) {
		Swal({
		  title: '<strong>' + name + '</strong>',
		  html:html,
		  showCloseButton: true,
		  showCancelButton: false,
		  focusConfirm: false,
		  padding: 30,
		  showConfirmButton: false,
		  animation: true,
		}).then((result) => {  
	    	window.history.pushState('', 'Charleston, WV Restaurant Week', '/');
		});
	}
});

/**
 *
 * Filtering 25/35/All
 *
 */

 $(document).ready(function() {
 	var showAll = $('.show-all'),
 		show25 = $('.show-25'),
 		show35 = $('.show-35');

 	var cost25 = $('.cost-25'),
 		cost35 = $('.cost-35');

 	showAll.on('click', function() {
 		showAll.addClass('selected');
 		show25.removeClass('selected');
 		show35.removeClass('selected');

 		cost35.show();
 		cost25.show();
 	});

 	show25.on('click', function() {
 		showAll.removeClass('selected');
 		show25.addClass('selected');
 		show35.removeClass('selected');

 		cost35.hide();
 		cost25.show();
 	});

 	show35.on('click', function() {
 		showAll.removeClass('selected');
 		show25.removeClass('selected');
 		show35.addClass('selected');

 		cost35.show();
 		cost25.hide();
 	});

 });


 /**
  *
  * Powering random button
  *
  */
 
 $(document).ready(function() {
 	var randomButton = $('.random'),
 		restHolder = $('.rest-holder');

 	randomButton.on('click', function() {
 		var random = restHolder.random().children().click();
 	});

	jQuery.fn.random = function() {
	    var randomIndex = Math.floor(Math.random() * this.length);  
	    return jQuery(this[randomIndex]);
	};

 });