<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="great-white-box">
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

			?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<h2 class="center section__title single__title"><?php the_title(); ?></h2>
                <div class="section__content single__content">
                    <p class="lead"><?php the_content(); ?></p>
                </div>

				</article><!-- #post-## -->

			<?php
			endwhile; // End of the loop.
			?>
</div><!-- .wrap -->

<?php get_footer();
